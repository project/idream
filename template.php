<?php

function om_menu_tree($tree) {
    return '<ul class="menu">'. $tree .'</ul>';
}

/** 
 * Logo, Site Name, Site Slogan
 */
function om_identity_spanned($logo, $site_name, $site_slogan, $front_page) {
  if (!empty($logo) || !empty($site_name) || !empty($site_slogan)) { 
    $out = '<div id="logo-title">';
    if (!empty($logo)) { 
      $out .= '<a href="' . $front_page . '" title="' . t('Home') . '" rel="home" id="logo">';
      $out .= '<img src="' . $logo . '" alt="' . t('Home') . '" />';
      $out .= '</a>';
    }
    if (!empty($site_name) || !empty($site_slogan)) { 
      $out .= '<div id="name-and-slogan">';
      if (!empty($site_name)) {
        $out .= '<h1 id="site-name">';
        $out .= '<a href="' . $front_page . '" title="' . t('Home') . '" rel="home">' . $site_name . '</a>';
        $out .= '</h1>';
      }
      if (!empty($site_slogan)) {
        $out .= '<span id="site-slogan">' . $site_slogan . '</span>';
      }
      $out .= '</div> <!-- /#name-and-slogan -->';
   }    
   $out .= '</div> <!-- /#logo-title -->';
  return $out;
  }
}